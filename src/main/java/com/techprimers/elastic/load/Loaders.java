package com.techprimers.elastic.load;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.techprimers.elastic.jparepository.UserJpaRepository;
import com.techprimers.elastic.model.Users;
import com.techprimers.elastic.repository.UsersRepository;

@Component
public class Loaders {

	@Autowired
	ElasticsearchOperations operations;
	
	@Autowired
	UsersRepository usersRepository;
	
	@Autowired
	UserJpaRepository userJpaRepository;
	
	@PostConstruct
	@Transactional
	public void loadAll() {
		operations.putMapping(Users.class);
		System.out.println("Loading Data");
		List<Users> data = getData();
		userJpaRepository.deleteAll();
		userJpaRepository.saveAll(data); // Saves to postgresql db
		
		List<Users> usersList = userJpaRepository.findAll(); // get from DB
		usersRepository.deleteAll();
		usersRepository.saveAll(usersList); // loads into Elastic
		//usersRepository.saveAll(getData()); //usersRepository.save(getData());
		System.out.println("Loading completed");
	}

	private List<Users> getData() {
		List<Users> userses = new ArrayList<>();
		userses.add(new Users("Ajay", 123L, "Accounting", 12000L));
		userses.add(new Users("Techie", 1230L, "Accounting", 12000L));
		userses.add(new Users("Jaga", 1234L, "Finance", 22000L));
		userses.add(new Users("Shiva", 124L, "Tech", 21000L));
		userses.add(new Users("Karthick", 14L, "Tech", 21000L));
		userses.add(new Users("Bhuvanesh", 4L, "Accounting", 21000L));
		userses.add(new Users("Kumaran", 1L, "Tech", 22000L));
		userses.add(new Users("Thiru", 1235L, "Accounting", 12000L));
		return userses;
	}
	
}
