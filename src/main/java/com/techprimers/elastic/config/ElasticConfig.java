package com.techprimers.elastic.config;

import java.io.File;
import java.io.IOException;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "com.techprimers.elastic.jparepository")
@EnableElasticsearchRepositories(basePackages = "com.techprimers.elastic.repository")
public class ElasticConfig {

	@Bean
	public ElasticsearchOperations elasticsearchTemplate() throws IOException {
		File tmpDir = File.createTempFile("elastic", Long.toString(System.nanoTime()));
		System.out.println("Temp directory: " + tmpDir.getAbsolutePath());
		
		Settings elasticsearchSettings = 
				Settings.builder()
						.put("http.enabled", "true") // 1
						.put("index.number_of_shards", "1")
						.put("path.data", new File(tmpDir, "data").getAbsolutePath())
						.put("path.logs", new File(tmpDir, "logs").getAbsolutePath())
						.put("path.work", new File(tmpDir, "work").getAbsolutePath())
						.put("path.home", tmpDir.getAbsolutePath()).build(); // 3
		
		TransportClient client = new PreBuiltTransportClient(elasticsearchSettings);
		return new ElasticsearchTemplate(client);
	}
}
